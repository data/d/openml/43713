# OpenML dataset: Weather-in-Szeged-2006-2016

https://www.openml.org/d/43713

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This is a dataset for a larger project I have been working on. My idea is to analyze and compare real historical weather with weather folklore.
Content
The CSV file includes a hourly/daily summary for Szeged, Hungary area, between 2006 and 2016.
Data available in the hourly response:

time
summary
precipType
temperature
apparentTemperature
humidity
windSpeed
windBearing
visibility
loudCover
pressure

Acknowledgements
Many thanks to Darksky.net team for their awesome API.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43713) of an [OpenML dataset](https://www.openml.org/d/43713). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43713/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43713/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43713/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

